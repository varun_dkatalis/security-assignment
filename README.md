# Security Assignment

This Assignment is in 2 sections.
- Multiple Choice Questions
- Practical

## Multiple Choice Questions

The Multiple Choice Questions are listed [here](https://docs.google.com/document/d/1TICnl1Xx0XNMMjK6WrB-RT6Rjmjs2Dt3aAH8IaI1S3c/)

You need to submit the answers [here](https://forms.gle/xSRrHFqmPmbcVTzP7)

## Practical

Visit the following Web application

http://167.71.229.175/dk_labs/

You will observe the seven labs mentioned as lab 1, lab 2, & so on till lab 7. Your task is to identify the vulnerability in these labs.

Note: Each lab has only one vulnerability.

Once you are able to identify the vulnerability you need to submit the following details about the vulnerability  in the google form.

In the text box you need to write the Labs number as shown below

```sh
Lab 1 / Challenge 1 :
Vulnerability Name:
Vulnerable parameter/s:
Payload used:
If possible POC:
Lab 2 / Challenge 2 :
Vulnerability Name:
Vulnerable parameter/s:
Payload used:
If possible POC:
Lab 3 / Challenge 3 :
Vulnerability Name:
Vulnerable parameter/s:
Payload used:
If possible POC:

.......
```